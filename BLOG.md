# Shift-Left DevSecOps with Atlassian, AWS & Lacework

Enterprise organizations recognize the need to build security into their software supply chains as they embark on complex digital transformation journeys. Digital transformation technologies like microservice architectures, multi-cloud infrastructure, containers and Kubernetes introduce new vectors for vulnerabilities. Lacework’s unique container and Kubernetes workload protection features allow your organization to embed security in your company’s software delivery pipeline from code build to deployment when using these technologies. Lacework allows you to embed security at multiple stages of your software supply chain and provide multiple redundant and overlapping layers of security. With Lacework in your software supply chain you can detect:

- vulnerabilities in your continuous integration and delivery (CI/CD) pipelines before software packages are released
- vulnerabilities in your container registries before software is deployed
- vulnerabilities in active containers in your Kubernetes environments
- anomalous and suspicious behavior in your Kubernetes environments

Additionally, lacework provides rich security context for these detection alerts that allow developers and security analysts to quickly identify and resolve the issue. These alerts can be sent to Jira for triage and resolution.

The best way to demonstrate Lacework’s security features is to build a CI/CD pipeline that delivers software to a Kubernetes cluster. We will build a simple container application using Bitbucket Cloud. Bitbucket Cloud provides a cloud-based Git repository and CI/CD features. This application uses NPM and is packaged as a container image. Such a software package can allow opportunities to introduce vulnerabilities from software and OS dependencies. The pipeline will push the container image to an AWS ECR container registry. Lacework further provides security protection for your container registries with security scans. Finally, the container image will be deployed to an AWS EKS cluster. A lightweight Lacework agent on the cluster provides runtime security protection. This runtime security protection helps identify vulnerabilities in active containers and identifies unusual activity in your Kubernetes clusters.

The code and pipeline for this demonstration code is located in this [Bitbucket Cloud repo](https://bitbucket.org/lacework-alliances/shift-left-devsecops-atlassian-lacework).

![Software Delivery Pipeline with Atlassian, AWS & Lacework](https://drive.google.com/uc?export=view&id=1r-LKQkYDap_qmsJU4cXIMh0S07HP3cqH)

## Prerequisites
To execute this demonstration, you must have an AWS, Bitbucket Cloud and Lacework accounts.

## AWS Setup
Before getting started with building our software, we must first set up our cloud infrastructure on Amazon AWS.

### AWS CLI and EKSCTL
The AWS CLI and EKSCTL will be used to create and access our EKS cluster. Follow these platform-specific [installation instructions](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) to install the AWS CLI. Next, install EKSCTL with [these instructions](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html).

### AWS IAM Permissions
If you don’t yet have an AWS user with programmatic access, [create one and generate an access key](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html). To create our EKS cluster, we require specific IAM permissions. Add these [minimum IAM permissions](https://eksctl.io/usage/minimum-iam-policies/) to the AWS user [using IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_manage-attach-detach.html). And then for AWS ECR, add the _AmazonEC2ContainerRegistryPowerUser_ permission to user.

![Required IAM Permissions](https://drive.google.com/uc?export=view&id=1Nt0XTVe2D0NaHezJlioAkU-PRDSfbpVv)

Authenticate your AWS CLI by running 'aws configure'. Provide your access key ID, access key and default region.

![aws configure Command](https://drive.google.com/uc?export=view&id=1Ywoa5CvLe0IfHDQ6-VvUeY7ifw2gXTQs)

### AWS EKS
Elastic Kubernetes Service (EKS) is AWS’s cloud Kubernetes platform. We will deploy our application to an EKS cluster. If you don’t yet have an AWS EC2 SSH key setup, follow these instructions to create one. EKS uses EC2 instances for Kubernetes nodes.

Use the following EKSCTL command to create an EKS cluster using default parameters. This default command will create a Kubernetes cluster with one managed nodegroup containing two Ec2 m5.large nodes.

```
eksctl create cluster \
--name <cluster name> \
--region <region> \
--with-oidc \
--ssh-access \
--ssh-public-key <SSH key name>
```

Cloudformation is used to create the cluster. In addition to the output from the above command, you can go to your Cloudformation console to monitor the creation of the cluster.

![AWS Cloudformation Console](https://drive.google.com/uc?export=view&id=18bijHgK13eYXtJeF-EeiC3yxBupPQR3I)

When complete, the cluster will appear in the [AWS EKS Console](https://console.aws.amazon.com/eks/).

![aws-cloudformation-eks](https://drive.google.com/uc?export=view&id=1U4ZbuakvZ72qxNUIwaCP_yPucS4iyWHZ)

To test the cluster and also create a namespace for our application, execute the following command.

``
kubectl create namespace demo-app
``

### Lacework Kubernetes Agent
The Lacework Kubernetes agent is a lightweight and passive application that monitors the activity on the Kubernetes cluster. It is key for detecting vulnerabilities and potentially bad behaviors in your Kubernetes clusters. From your Lacework console, create a new Agent Access Token and download the Kubernetes Config and Kubernetes Orchestration files.

![Lacework K8s Agent Setup](https://drive.google.com/uc?export=view&id=19k92gF5uWtoMlfdaTbZGB2JsRvJqpYk2)

Then apply these to your cluster with the following commands.

``
kubectl create -f lacework-cfg-k8s.yaml
``

``
kubectl create -f lacework-k8s.yaml
``

![Apply Lacework K8s Agent Manifests](https://drive.google.com/uc?export=view&id=1a8BzJY4QzWbMfo14Xklf_4lQh-48aLfX)

### AWS ECR
Amazon Elastic Container Registry (Amazon ECR) provides an easy-to-use cloud container registry. We will use Amazon ECR to provide a repository for our application. We will then deploy from the repository to our EKS cluster. Follow these [instructions to create a new private repository](https://docs.aws.amazon.com/AmazonECR/latest/userguide/repository-create.html) and name it _shift-left-devsecops-atlassian-lacework/demo-app_.

![ecr-repo](https://drive.google.com/uc?export=view&id=1Q7DIR7LoxF2AiqRPFC5_xWn9zJT5llUv)

We will use a Kubernetes deployment file to deploy our application. We reference a Kubernetes secret _regcred_ in that deployment file that contains the registry credentials for our ECR registry. Create this secret in our cluster with the following command.

```
kubectl create secret docker-registry regcred \
  --docker-server=<ECR registry, ie. 911290716430.dkr.ecr.us-west-2.amazonaws.com> \
  --docker-username=AWS \
  --docker-password=$(aws ecr get-login-password) \
  --namespace=demo-app
```

### Lacework Container Registry Scanning
Lacework can scan your container registries as another layer of added protection to prevent vulnerable software from being deployed. Your DevOps teams may miss including software scanning in their CI/CD pipelines or vulnerable software may be pushed directly to your repositories. Lacework can alert you to these incidents.

From your Lacework console, create a new Container Registry entry and specify _Amazon Container Registry (ECR)_ with _AWS Key ID Access Key_. 

![Lacework Amazon ECR Container Scanning Setup](https://drive.google.com/uc?export=view&id=13ouo2KVpCqvAZCKXNKDaTLd8Y-1zJfO7)

Then enter your access key credentials and registry domain.

![Lacework Amazon ECR Container Scanning Setup](https://drive.google.com/uc?export=view&id=1yeemgqDYHSdFKKa5BaQjD_KC8JUbPgZU)

Lacework will now scan your repositories for vulnerabilities.

### Bitbucket Cloud
The example code is hosted in Bitbucket Cloud and you can [fork](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/) the [Bitbucket Cloud repo](https://bitbucket.org/lacework-alliances/shift-left-devsecops-atlassian-lacework) to your workspace and project. 

![Fork Code Repository](https://drive.google.com/uc?export=view&id=17g6bWuuwb_GwPKT7GGflxZtsrcm9pMoa)

#### Repository Variables
Several _Repository Variables_ are used by the CI/CD pipeline to build and deploy our application. Set these variables to your specific instances under _Repository Settings_.

- LW_ACCOUNT_NAME - This is your Lacework account name.
- LW_ACCESS_TOKEN - This is an API access token that you can generate with [these instructions](https://support.lacework.com/hc/en-us/articles/360011403853-Generate-API-Access-Keys-and-Tokens).
- AWS_ACCESS_KEY_ID - This is the ID for the access that you generated for your AWS user.
- AWS_SECRET_ACCESS_KEY - This is the access key that you generated for your AWS user.
- AWS_DEFAULT_REGION - This is your default AWS region.
- EKS_CLUSTER - This is the name of the EKS cluster that your created.
- DOCKER_REG - This is the domain of the ECR registry that you created.
- IMAGE_NAME - This is the name of the image that the pipeline will build.

![Repository Variables](https://drive.google.com/uc?export=view&id=1nLttidHzAm18flvzRYFKOdZ1eA2xSnZn)

#### Bitbucket Pipelines
We use Bitbucket Pipelines as our CI/CD engine to build and deploy our application. The pipeline is defined in a YAML file, [bitbucket-pipelines.yml](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/). The file consists of steps and script commands that execute using pre-defined or customer container environments. Before executing our pipeline, let's review the sections in our [pipeline file](https://bitbucket.org/lacework-alliances/shift-left-devsecops-atlassian-lacework/src/master/bitbucket-pipelines.yml).

The first line in the file defines the default container image to use for step execution.
```
image: atlassian/default-image:2
```

These next lines define our pipeline and the first step. In this first step, we build our docker image. We then use the lacework-scan pipe to scan for software and OS vulnerabilities. We set the variables FAIL_BUILD=false to not fail this build if vulnerabilities are found. This is only done for the purposes of this demonstration and to allow deployment. In production, you should fail your builds and prevent them from being pushed to registries and deployed. The we use the aws-ecr-push-image pipe to push the image to our ECR repository.

```
pipelines:
  default:
    - step:
        name: Docker Build-Push with Lacework Vulnerability Scan
        script:
          - cd demo-app
          - docker build . -t "${IMAGE_NAME}:${BITBUCKET_BUILD_NUMBER}"
          - pipe: docker://lacework/lacework-scan:latest
            variables:
              LW_ACCOUNT_NAME: ${LW_ACCOUNT_NAME}
              LW_ACCESS_TOKEN: ${LW_ACCESS_TOKEN}
              IMAGE_NAME: ${IMAGE_NAME}
              IMAGE_TAG: ${BITBUCKET_BUILD_NUMBER}
              FAIL_BUILD: "false"
          - pipe: atlassian/aws-ecr-push-image:1.4.2
            variables:
              AWS_ACCESS_KEY_ID: "${AWS_ACCESS_KEY_ID}"
              AWS_SECRET_ACCESS_KEY: "${AWS_SECRET_ACCESS_KEY}"
              AWS_DEFAULT_REGION: "${AWS_DEFAULT_REGION}"
              IMAGE_NAME: "${IMAGE_NAME}:${BITBUCKET_BUILD_NUMBER}"
              TAGS: "${BITBUCKET_BUILD_NUMBER} latest"
              DEBUG: "true"
        services:
          - docker
        caches:
          - docker
```

In the next step, we deploy our application to our EKS cluster. Our first script commands authenticate with AWS and our EKS cluster. We then update our [Kubernetes deployment manifest](https://bitbucket.org/lacework-alliances/shift-left-devsecops-atlassian-lacework/src/master/demo-app/deployment.yml) with our new image name. We then apply the manifest to deploy our application and wait for it to come up. We use a different image in this step to give us the tools to do this.
```
    - step:
        name: Deploy to EKS Cluster
        image: alpine/k8s:1.20.7
        deployment: Production
        script:
          - export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
          - export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
          - export AWS_DEFAULT_REGION="${AWS_DEFAULT_REGION}"
          - aws eks --region "${AWS_DEFAULT_REGION}" update-kubeconfig --name "${EKS_CLUSTER}"
          - kubectl get svc
          - sed "s|imageName|${DOCKER_REG}/${IMAGE_NAME}:${BITBUCKET_BUILD_NUMBER}|g" demo-app/deployment.yml > my-deployment.yml
          - cat my-deployment.yml
          - kubectl apply -f my-deployment.yml --namespace demo-app
          - while [ -z "$url" ]; do url=$(kubectl describe service demo-app --namespace demo-app | grep 'LoadBalancer Ingress:' | awk '{printf "https://%s",$3;}'); sleep 2; done
          - echo "$url"
          - echo "Demo App launched!"
```

This last section increases the amount of memory to be used by the docker service.
```
definitions:
  services:
    docker:
      memory: 4096
```

We can now execute the pipeline. Use the _Run pipeline_ button under _Pipelines_ in your Bitbucket Cloud repository.

![Bitbucket Pipeline Execution](https://drive.google.com/uc?export=view&id=1MPgrY2YcuFj0JKw3K8fytZRi2NGCLL1j)

Click on the run to view the build process. In the first step, you can see the image built and Lacework discovers several vulnerabilities. Again, we should allow the build to fail here, but we will let it continue.

![Build-Push with Lacework Scan Step](https://drive.google.com/uc?export=view&id=1WprYyoMtatZSZM5s4Li7ABiucqEjnmzd)

In the second step, we deploy the application to our EKS cluster using the deployment manifest. Click on the _URL_ to view the application.

![Deploy Pipeline Step](https://drive.google.com/uc?export=view&id=1GtI2K6m8y33SJH_NIiWA4vK9e1p60q_i)

![Demo Application Deployment](https://drive.google.com/uc?export=view&id=1GBfyMHdmEh1QgJmmPSOEzMvz5d6921Cq)

### Lacework Container and Kubernetes Security

We saw earlier how Lacework can detect vulnerabilities in the CI/CD pipeline. But we also set up Lacework to scan our registry and monitor our Kubernetes cluster. This provides additional security at multiple stages of our software delivery.

In the Lacework console, go to _Vulnerabilities > Containers_. Lacework detected vulnerabilities in our application image from our registry.

![Container Vulnerability Assessment](https://drive.google.com/uc?export=view&id=1dy-_DFm8SbXDa6_fDB_d62JNrunIeCor)

If you ran the pipeline multiple times, multiple images are generated and scanned by Lacework. You can also see that there are active containers which is critical to identifying active vulnerabilities. Click on one of the entries.

Scroll down to the Lacework Polygraph to see the _Application Communication_ to see the network communication to the container. If the vulnerability exposes your software to external bad actors, this is where you can see that activity.

![Polygraph - Application Communication](https://drive.google.com/uc?export=view&id=1w5yfdLFVdGX_REvxNm30HzNUj6wZQRJM)

Scroll further down to see the container pod name, namespace and K8s node IPs that are impacted. This can help you determine which EKS clusters and namespaces to focus on to resolve the security incident.

![Active Containers](https://drive.google.com/uc?export=view&id=1abyX0wBbk_kXwLuwqYb8Tnit_Q2vzkEz)

In this blog post, you learned how you can integrate security into multiple stages of your software delivery pipeline from identifying vulnerabilities in your build stage to detecting vulnerabilities in your software repositories to identifying vulnerabilities in your active containers. This provides multiple layers of overlapping security to ensure your software delivery pipeline delivers secure software. To learn more about the Lacework Platform, go to [https://www.lacework.com/platform-overview/](https://www.lacework.com/platform-overview/).