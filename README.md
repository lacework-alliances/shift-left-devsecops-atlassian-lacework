## Shift-Left DevSecOps with Atlassian and Lacework

This GitHub repository contains the code that demonstrates how to use Atlassian Bitbucket Cloud and Lacework to build, secure, test and deploy an NPM application. For more detail, check out the [blog post](BLOG.md).

![pipeline](x1r-LKQkYDap_qmsJU4cXIMh0S07HP3cqH)

# Demo App Local Development

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.

![demo-app](https://drive.google.com/uc?export=view&id=1GBfyMHdmEh1QgJmmPSOEzMvz5d6921Cq)

## Build and Run Docker Image Locally

```
$ docker build -t demo-app . 
$ docker run -p 443:443 -p 80:80 docker.io/library/demo-app
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## License

This library is licensed under the MIT-0 License. See the LICENSE file.

